const http = require('http'),
    fs = require('fs'),
    path = require('path');

const port = process.env.PORT || 3000;


http.createServer((request, response) => {
  console.log(`${request.method} ${request.url}`);

  let fileName = 'index.html';
  if (request.url == '/ver.html') {
    if (fs.existsSync('ver.html')) {
      fileName = 'ver.html';
    }
  }

  const filePath = path.join(__dirname, fileName);
  response.writeHead(200, {
    'Content-Type': 'text/html',
  });
  
  const readStream = fs.createReadStream(filePath);
  readStream.pipe(response);
})
.listen(port);

console.log(`server listening port ${port}`);

