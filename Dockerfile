FROM node:8

ENV PORT 8080

EXPOSE 8080

WORKDIR /test-devops
ADD . .

RUN yarn install

ENTRYPOINT yarn start